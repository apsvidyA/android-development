fun main(){
    var wholeNumber = 10
    var fractionalNumber = 3.5
    var sentence = "A new sentence"
    var condition = false
    var fnumber = 4.6f

    var a: Int = 2
    var str: String = "werty"

    println(wholeNumber)
    println(fractionalNumber)
    println(sentence)
    println(condition)

    val number = 2345
    val decimal = 5.6
    val name = "Internshala"
    val conditions = true
    val fnum = 5.6f



    wholeNumber = wholeNumber +10
    fractionalNumber = 34.8
    sentence = "Another sentence"
    condition = true

    println(wholeNumber)
    println(fractionalNumber)
    println(sentence)
    println(condition)
    println(fnumber)

    println(number)
    println(decimal)
    println(name)
    println(conditions)
    println(fnum)

    println(a)
    println(str)
}